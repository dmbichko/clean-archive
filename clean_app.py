import sys
import tempfile
import os
import shutil
import logging
import zipfile


def main():
    name_del_dir = 'cleaned.txt'
    name_arch = sys.argv[1]
    name_find_file = "__init__.py"

    logging.basicConfig(
        filename='app.log',
        level=logging.INFO,
        format='%(levelname)s:%(asctime)s:%(message)s')

    with zipfile.ZipFile(name_arch, 'r') as zip_ref:
        with tempfile.TemporaryDirectory() as tmpdir:
            result = os.path.exists(tmpdir)
            logging.info('Created temporary directory %r is %r', tmpdir, result)
        zip_ref.extractall(tmpdir)

    root = 0
    dir_del_part = []
    flag_skip = 1

    # Find and delete directories than doesn't include name_find_file
    for dirpath, dirnames, files in os.walk(tmpdir):
        if root <= 2:
            root_path = dirpath
            root += 1
            if root == 1:
                path_file = dirpath
        for file_name in files:
            if file_name == name_find_file:
                flag_skip = 1
        if flag_skip == 0 and root >= 1:
            logging.info('This directory %r will delete and add\
            special file %r ', dirpath, name_del_dir)
            shutil.rmtree(dirpath)
            dirpath = dirpath.replace(tmpdir, '')
            rm_path = dirpath[1:]
            dir_del_part.append(rm_path)
        flag_skip = 0

    # Add path to deleted file in srecial infomation file
    file_path = os.path.join(path_file, name_del_dir)
    f = open(file_path, 'w')
    if len(dir_del_part) != 0:
        dir_del_part.sort()
        for dir_str in dir_del_part:
            f.write(dir_str + '\n')
    f.close()

    new_arch_name = name_arch.replace('.zip', '') + '_new'
    shutil.make_archive(new_arch_name, 'zip', path_file)
    logging.info('%r created in %r directory ', new_arch_name, root_path)


if __name__ == "__main__":
    main()
